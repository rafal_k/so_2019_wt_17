package simulation.presenter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Presenter {
    private int liczba_procesorow;
    private int liczba_procesow;
    private ArrayList<Algorytm> results;

    public Presenter() {
        results = new ArrayList<>();
    }


    public int getLiczba_procesow() {
        return liczba_procesow;
    }

    public void setLiczba_procesow(int liczba_procesow) {
        this.liczba_procesow = liczba_procesow;
    }


    public void saveData(String name, double time) {
        Algorytm alg = new Algorytm(name, time);
        results.add(alg);

    }


    public int getLiczba_procesorow() {
        return liczba_procesorow;
    }

    public void setLiczba_procesorow(int liczba_procesorow) {
        this.liczba_procesorow = liczba_procesorow;
    }


    public void print() {
        for (Algorytm x: results)
            System.out.println("Sredni czas dla algotytmu: " + x.getName() + " jest równy   " + x.getTime());
    }

    public void start() {
        boolean czyPoprawne = false;

        while (czyPoprawne!= true) {
            try {
                System.out.println("Ile będzie procesorów?");
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                liczba_procesorow = Integer.parseInt(reader.readLine());
                czyPoprawne = true;
            }
            catch (Exception e) {
                System.out.println("Błąd! Liczba procesorów musi być liczbą naturalną.");
            }
        }

        czyPoprawne = false;

        while (czyPoprawne!= true) {
            try {
                System.out.println("Ile będzie procesów? ");
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                liczba_procesow = Integer.parseInt(reader.readLine());
                czyPoprawne = true;
            }

            catch (Exception e) {
                System.out.println("Błąd! Liczba procesów musi być liczbą naturalną.");
            }
        }
    }




    public void saveDataInFile()  {
        Writer output = null;
        try {
            output = new BufferedWriter(new FileWriter("text.txt", false));
            output.append("Symulacja algorytmów planowania dostępu " + this.getLiczba_procesow() + " procesów"+" do " + this.getLiczba_procesorow() + " procesorów: \n");

            for (Algorytm x: results) {
                ((BufferedWriter) output).newLine();
                output.append("Sredni czas dla algorytmu " + x.getName() + ": " + x.getTime() + "\n");
            }
            output.close();
        }
        catch (Exception e) {
            e.getMessage();
        }
    }


}