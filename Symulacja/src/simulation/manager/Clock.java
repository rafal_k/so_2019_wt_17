package simulation.manager;
import java.util.ArrayList;

public class Clock implements Runnable {

    private ArrayList<Tick> tickObservers = new ArrayList<>();
    private long currentTick = 0;
    private boolean continueLoop = true;
    
    @Override
    public void run () {
        updateTick();
    }
    
    private void updateTick(){
        currentTick = 0;
        while(continueLoop){
            
            notifyAll(currentTick++);
        }
    }
    
    private void notifyAll(long tick){
        for(Tick t: tickObservers){
            t.tick(tick);
        }
    }
    
    public void stop(){
        continueLoop = false;
    }
    
    public void addObserver(Tick t){
        tickObservers.add(t);
    }
    
    public void removeObserver(Tick t){
        tickObservers.remove(t);
    }
    
}
