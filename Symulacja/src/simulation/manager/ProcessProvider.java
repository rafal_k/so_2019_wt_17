package simulation.manager;

import simulation.dataStructures.LinkedList;
import simulation.schedulingAlgorithm.SchedulingAlgorithm;
import simulation.taskGenerator.Task;

import java.util.ArrayList;

public class ProcessProvider implements Tick {
    
    private LinkedList<Task> queue;
    private SchedulingAlgorithm algorithm;
    private double sum = 0.0;
    private long currentTick;
    private int doneCount = 0;
    
    public ProcessProvider(LinkedList<Task> queue, SchedulingAlgorithm algorithm){
        this.queue = queue;
        this.algorithm = algorithm;
    }
    
    public ArrayList<Task> getTaskList(int n){
        ArrayList<Task> tasks = algorithm.findNext(queue,n);
        doneCount += tasks.size();
        for(Task t: tasks){
            sum+= (currentTick - t.getTime());
        }
        return tasks;
    }
    
    public double getSum(){return sum;}
    
    public int getDoneCount () {
        return doneCount;
    }
    
    @Override
    public void tick (long currentTick) {
        this.currentTick = currentTick;
    }
}
