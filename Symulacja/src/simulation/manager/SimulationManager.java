package simulation.manager;
import simulation.dataStructures.LinkedList;
import simulation.presenter.Presenter;

import simulation.processor.Service;
import simulation.schedulingAlgorithm.SchedulingAlgorithm;
import simulation.taskGenerator.Task;
import simulation.taskGenerator.TaskGenerator;

import java.io.IOException;

public class SimulationManager implements Tick{
    
    private int cpuNumber;
    private int processNumber;
    private SchedulingAlgorithm algorithm;
    
    private Clock clock;
    private Thread thread;
    private LinkedList<Task> queue;
    private ProcessProvider provider;
    
    private Service taskManager;
    private Presenter presenter;
    private TaskGenerator taskGenerator;
    
    public SimulationManager (SchedulingAlgorithm algorithm){
        presenter = new Presenter();
        queue = new LinkedList<>();
        this.algorithm = algorithm;
        
        try{
            presenter.start();
        }catch (IOException e){
            System.out.println("Error");
        }
        
        cpuNumber = presenter.getLiczba_procesorow();
        processNumber = presenter.getLiczba_procesow();
        
        clock = new Clock();
        thread = new Thread(clock);
        taskGenerator = new TaskGenerator(clock,queue,processNumber);
        clock.addObserver(this);
        provider = new ProcessProvider(queue, algorithm);
        taskManager = new Service(cpuNumber,provider);
        thread.start();
    }
    
    public void startSimulation(SchedulingAlgorithm algorithm){
        this.algorithm = algorithm;
        cleanUp();
        thread.start();
    }
    
    private void cleanUp(){
        clock.removeObserver(taskManager);
        thread = new Thread(clock);
        queue = new LinkedList<>();
        provider = new ProcessProvider(queue,algorithm);
        taskManager = new Service(cpuNumber,provider);
    }
    
    private void printResults(){
        double average = provider.getSum()/provider.getDoneCount();
        presenter.saveData(algorithm.getName(),average);
        presenter.print();
        //presenter.SaveData(algorithm.getName(),average);
    }
    
    public void setAlgorithm(SchedulingAlgorithm algorithm){
        provider = new ProcessProvider(queue, algorithm);
    }
    
    @Override
    public void tick (long currentTick) {
        //System.out.println(currentTick);
        if(provider.getDoneCount() >= processNumber){
            clock.stop();
            printResults();
        }
    }
}