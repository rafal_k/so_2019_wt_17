package simulation.processor;

import simulation.manager.ProcessProvider;
import simulation.manager.Tick;
import simulation.taskGenerator.Task;
import java.util.ArrayList;

public class Service implements Tick {
    
    private ProcessProvider provider;
    private ArrayList<CPU> CPUs;
    private ArrayList<Task> tasksToProcess;

    public Service(int noOfCPU, ProcessProvider provider) {
        this.provider = provider;
        CPUs = new ArrayList<>();
        for(int i=0; i<noOfCPU; i++){
           CPUs.add(new CPU());
        }
    }
    
    @Override
    public void tick(long currentTick) {
        checkIfProcceded();
        int requiredProcessAmount = getFreeCPUsSize();
        if(requiredProcessAmount > 0){
            tasksToProcess = provider.getTaskList(requiredProcessAmount);
            allocateTasks(tasksToProcess);
        }
        lowerTaskTimeExecution();
    }
    
    public ArrayList<Task> getTasksToProcess() {
        return tasksToProcess;
    }

    public void setTasksToProcess(ArrayList<Task> tasksToProcess) {
        this.tasksToProcess = tasksToProcess;
    }
    
    private void allocateTasks(ArrayList<Task> tasks) {
        if(tasks.size() == 0){
            return;
        }
        for(int i=0; i<tasks.size(); i++){
           for(CPU cpu : CPUs){
               if(cpu.getExecutedTask() == null){
                   cpu.setExecutedTask(tasks.get(i));
                   break;
               }
           }
        }
    }

    public void clearAll(){
        checkIfProcceded();
        for(CPU cpu: CPUs){
            cpu.setExecutedTask(null);
        }
    }
    
    private void lowerTaskTimeExecution(){
        for(CPU cpu: CPUs){
            if(cpu.getExecutedTask() != null){
                cpu.getExecutedTask().decrement();
            }
        }
    }

    private void checkIfProcceded(){
        for(CPU cpu : CPUs){
            if(cpu.getExecutedTask() != null){
                if(cpu.getExecutedTask().getRequiredTime() == 0){
                    cpu.setExecutedTask(null);
                    provider.incrementDone();
                }
            }
        }
    }
    
    private int getFreeCPUsSize(){
        int i = 0;
        for(CPU cpu : CPUs){
            if(cpu.getExecutedTask() == null) i++;
        }
        return i;
    }

    public ArrayList<Task> getTasksInProgress(){
        ArrayList<Task> tasksInProgress = new ArrayList<>();
        for(CPU cpu : CPUs){
            if(cpu.getExecutedTask() != null){
                tasksInProgress.add(cpu.getExecutedTask());
            }
        }
        return tasksInProgress;
    }

    public void swapTasks(Task taskInProgress, Task toSwap){
        for(CPU cpu : CPUs){
            if(cpu.getExecutedTask().equals(taskInProgress)) {
                cpu.setExecutedTask(toSwap);
                return;
            }
        }
    }
    
    private class CPU {
        
        private Task currentTask;
        public CPU(){
            currentTask = null;
        }
        
        public Task getExecutedTask() {
            return currentTask;
        }
        
        public void setExecutedTask(Task currentTask) {
            this.currentTask = currentTask;
        }
        
        public boolean isBusy() {
            if (currentTask == null) return false;
            else return true;
        }
    }
    
}