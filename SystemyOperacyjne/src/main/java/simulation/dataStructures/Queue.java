package simulation.dataStructures;

public interface Queue<E> {
    boolean isEmpty();
    boolean clear();
    E dequeue();
    boolean enqueue(E element);
    int size();
    E peekNext();
    E peek(int position);
}
