package simulation.taskGenerator;
import java.io.*;
import java.util.ArrayList;

public class Logger implements Serializable {
    
    private String fileName;
    private ArrayList<Task> tasks;
    
    public Logger(String fileName){
        this.fileName = fileName;
        tasks = new ArrayList<>();
    }
    
    public void getTask(ArrayList<Task> tasks){
        this.tasks = tasks;
    }
    
    public void saveTasks(){
        
        try{
            FileWriter fileWriter = new FileWriter(new File(fileName));
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for(Task t: tasks){
                bufferedWriter.write(t.getId()+"\t"+t.getRequiredTime()+"\t"+t.getTime()+"\n");
                bufferedWriter.flush();
            }
            
        }catch (FileNotFoundException e){
            System.out.println("No such file");
        }catch (IOException e){
            System.out.println("Writing error");
        }
        
    }
    
}
