package simulation.taskGenerator;
import simulation.manager.ProcessProvider;
import simulation.manager.Tick;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;

public class TaskGenerator implements Tick {
    
    private ArrayList<Task> tasks;
    private static HashSet<Integer> ids = new HashSet<>();
    private static ArrayList<Task> testTasks;
    private ProcessProvider list;
    private int numberOfGenerated = 0;
    private int numberOfProcesses;
    private int sum = 0;
    private int average;
    
    public TaskGenerator(int numberOfProcesses) {
         tasks = new ArrayList<>();
         this.numberOfProcesses = numberOfProcesses;
         generate();
         tasks.sort(new Comp());
         //printTasks();
    }
    
    public void setProvider(ProcessProvider list){
        this.list = list;
        list.setAverageTime(sum/numberOfProcesses);
    }
    
    private void generate(){
        int tact = 0;
        int limit = 0;
        if(numberOfProcesses < 20){
            limit = 5*numberOfProcesses+10;
        }else{
            limit = (int)(25*Math.sqrt(numberOfProcesses)+20);
        }
        while (numberOfGenerated < numberOfProcesses){
            int range = getProbability();
            if(range <= (numberOfProcesses-numberOfGenerated)){
                Random random = new Random();
                for(int i=0; i<range; i++){
                    Task task = new Task(random.nextInt(limit)+10,tact,generateId());
                    tasks.add(task);
                    sum += task.getRequiredTime();
                }
                tact++;
                numberOfGenerated += range;
            }
            else{
                Random random = new Random();
                for(int i=0; i<(numberOfProcesses-numberOfGenerated); i++){
                    Task task = new Task(random.nextInt(limit)+10,tact,generateId());
                    tasks.add(task);
                    sum += task.getRequiredTime();
                }
                tact++;
                numberOfGenerated = numberOfProcesses;
            }
        }
        Logger logger = new Logger("taskLog.txt");
        logger.getTask(tasks);
        logger.saveTasks();
    }
    
    private int generateId(){
        Random random = new Random();
        int generatedId = random.nextInt(10000)+100000;
        if(!ids.contains(generatedId)){
            ids.add(generatedId);
            return generatedId;
        }
        else  return generateId();
    }
    
    private int getProbability(){
        Random random = new Random();
        int range = random.nextInt(1001);
        if(range <= 420) return 0;
        else if(range <= 730) return 1;
        else if(range <= 880) return 2;
        else if(range <= 960) return 3;
        else if(range <= 990) return 4;
        else if(range <= 995) return 5;
        else if(range <= 997) return 6;
        else if(range <= 999) return 7;
        else if(range <= 1000) return 8;
        else return 0;
    }
    
    public void printTasks(){
        System.out.println("Czas wykonywania | Tick wejscia");
        for(Task t: tasks){
            System.out.println(t.getRequiredTime()+" | "+t.getTime());
        }
        System.out.println("**********************************");
    }
    
    @Override
    public void tick(long currentTick) {
        for (Task task : tasks) {
            if (task.getTime() == currentTick) {
               try{
                   list.add(task.clone());
               }catch (CloneNotSupportedException e){
                   System.out.println("Not able to clone");
               }
            }
        }
    }
    
    private class Comp implements Comparator<Task>{
        @Override
        public int compare (Task o1, Task o2) {
            Long a = o1.getTime();
            Long b = o2.getTime();
            return a.compareTo(b);
        }
    }
    
    static {
        testTasks = new ArrayList<>();
        testTasks.add(new Task(10,0,1));
        testTasks.add(new Task(5,0,2));
        testTasks.add(new Task(2,1,3));
        testTasks.add(new Task(5,1,4));
        testTasks.add(new Task(4,1,5));
        testTasks.add(new Task(3,3,6));
        testTasks.add(new Task(10,7,7));
    }
    
}