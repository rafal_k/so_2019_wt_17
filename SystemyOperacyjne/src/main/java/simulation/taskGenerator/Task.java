package simulation.taskGenerator;

public class Task implements Cloneable{
	 
    private int requiredTime;
    private int id;
    private long time;
 
    public Task(int requiredTime, int time, int id) {
        this.requiredTime = requiredTime;
        this.time = time;
        this.id = id;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Task) {
            return ((Task) obj).id == this.id;
        }
        return false;
    }
    
    @Override
    protected Task clone () throws CloneNotSupportedException {
       Task cloned = (Task)super.clone();
       return cloned;
    }
    
    public int getId () {
        return id;
    }
    
    public void decrement(){
        requiredTime--;
    }
    
    public int getRequiredTime() {
        return requiredTime;
    }
 
    public void setRequiredTime(int requiredTime) {
        this.requiredTime = requiredTime;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setId(int id) {
        this.id = id;
    }
}