package simulation;

import simulation.manager.SimulationManager;
import simulation.schedulingAlgorithm.*;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ArrayList<SchedulingAlgorithm> algorithms = new ArrayList<>(Arrays.asList(new FCFS(),new SJF(),new SJFOuster(),new Rotary(10)));
        SimulationManager manager = new SimulationManager();
        for(SchedulingAlgorithm algorithm: algorithms){
            manager.startSimulation(algorithm);
        }
        manager.printResults();
    }
}
