package simulation.manager;
import java.util.ArrayList;

public class Clock{

    private ArrayList<Tick> tickObservers = new ArrayList<>();
    private long currentTick = 0;
    private boolean continueLoop = true;
    
    public void start(){
        currentTick = 0;
        while(continueLoop){
            notifyAll(currentTick++);
        }
    }
    
    private void notifyAll(long tick){
        for(Tick t: tickObservers){
            t.tick(tick);
        }
    }
    
    public void clearObservers(){
        tickObservers =  new ArrayList<>();
    }
    
    public void stop(){
        continueLoop = false;
    }
    
    public void addObserver(Tick t){
        tickObservers.add(t);
    }
    
}
