package simulation.manager;

import simulation.dataStructures.LinkedList;
import simulation.presenter.Presenter;
import simulation.processor.Service;
import simulation.schedulingAlgorithm.SchedulingAlgorithm;
import simulation.taskGenerator.Task;
import simulation.taskGenerator.TaskGenerator;

import java.util.ArrayList;

public class SimulationManager implements Tick{
    
    private int cpuNumber;
    private int processNumber;
    private Service service;
    private Presenter presenter;
    
    private SchedulingAlgorithm algorithm;
    private ArrayList<Task> queue;
    private Clock clock;
    private ProcessProvider provider;
    private TaskGenerator taskGenerator;
    
    public SimulationManager (){
        presenter = new Presenter();
        presenter.start();
        cpuNumber = presenter.getLiczba_procesorow();
        processNumber = presenter.getLiczba_procesow();
        taskGenerator = new TaskGenerator(presenter.getLiczba_procesow());
    }
    
    public void startSimulation(SchedulingAlgorithm algorithm){
        this.algorithm = algorithm;
        queue = new ArrayList<>();
        clock = new Clock();
        clock.clearObservers();
        provider = new ProcessProvider(algorithm,queue);
        service = new Service(cpuNumber,provider);
        provider.setService(service);
        taskGenerator.setProvider(provider);
        
        clock.addObserver(taskGenerator);
        clock.addObserver(this);
        clock.addObserver(provider);
        clock.addObserver(service);
        clock.start();
    }
    
    private void sendResults(){
        double average = provider.getSum()/provider.getDoneCount();
        presenter.saveData(algorithm.getName(),average);
    }
    
    public void printResults(){
        presenter.print();
    }
    
    @Override
    public void tick (long currentTick) {
        if(provider.getDoneCount() == processNumber){
            clock.stop();
            sendResults();
        }
    }
}