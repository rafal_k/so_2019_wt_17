package simulation.manager;

import simulation.processor.Service;
import simulation.schedulingAlgorithm.SchedulingAlgorithm;
import simulation.taskGenerator.Task;
import java.util.ArrayList;

public class ProcessProvider implements Tick {
    
    private ArrayList<Task> queue;
    private SchedulingAlgorithm algorithm;
    private double sum = 0.0;
    private long currentTick;
    private int doneCount = 0;
    private Service service;
    private int averageTime = 0;
    
    public ProcessProvider(SchedulingAlgorithm algorithm, ArrayList<Task> queue){
        this.queue = queue;
        this.algorithm = algorithm;
    }
    
    public ArrayList<Task> getTaskList(int n){
        ArrayList<Task> tasks = algorithm.findNext(queue,n);
        for(Task t: tasks){
            sum += (currentTick - t.getTime());
        }
        return tasks;
    }
    
    public void setAverageTime (int averageTime) {
        this.averageTime = averageTime;
    }
    
    public void add(Task t) {
        
        if(algorithm.isOuster() && algorithm.quantum()==null){
           ArrayList<Task> currentTasks = service.getTasksInProgress();
            int min = t.getRequiredTime();
            Task temp = null;
            for(Task task: currentTasks){
                if(task.getRequiredTime()>min){
                    temp = task;
                    min = task.getRequiredTime();
                }
            }
            if (temp != null){
                service.swapTasks(temp,t);
                queue.add(temp);

                temp.setTime(currentTick);
            }
            else queue.add(t);
        }
        else queue.add(t);
    }
    
    public void incrementDone(){
        doneCount++;
    }
    
    public void setService(Service service){
        this.service = service;
    }
    
    public double getSum(){return sum;}
    
    public int getDoneCount () {
        return doneCount;
    }
    
    @Override
    public void tick ( long currentTick){
        this.currentTick = currentTick;
        
        if(algorithm.quantum() != null){
            if(currentTick % ((averageTime*3)/4) == 0){
                ArrayList<Task> tasks = service.getTasksInProgress();
                for(Task t: tasks){
                    t.setTime(currentTick);
                    queue.add(t);
                }
                service.clearAll();
            }
        }
    }

}
