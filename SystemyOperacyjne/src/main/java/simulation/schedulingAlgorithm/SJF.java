package simulation.schedulingAlgorithm;

import simulation.dataStructures.LinkedList;
import simulation.taskGenerator.Task;

import java.util.ArrayList;

public class SJF implements SchedulingAlgorithm {
    
    @Override
    public ArrayList<Task> findNext (ArrayList<Task> list, int n) {
       ArrayList<Task> temp = new ArrayList<>();
       if(list == null || n<=0) return temp;
       
       if(n >= list.size()){
           for(int i = 0; i < list.size(); i++){
                temp.add(findMin(list));
           }
       }
       else{
           for(int i = 0; i < n; i++){
               temp.add(findMin(list));
           }
       }
       return temp;
    }
    
    private Task findMin(ArrayList<Task> list){
        Task min = list.get(0);
        int index = 0;
        for(int i=1 ;i<list.size();i++){
            if(list.get(i).getRequiredTime() < min.getRequiredTime()){
                min = list.get(i);
                index = i;
            }
        }
        list.remove(index);
        return min;
    }
    
    @Override
    public String getName () {
        return "SJF";
    }
    
    @Override
    public boolean isOuster () {
        return false;
    }
    
    @Override
    public Integer quantum () {
        return null;
    }
}
