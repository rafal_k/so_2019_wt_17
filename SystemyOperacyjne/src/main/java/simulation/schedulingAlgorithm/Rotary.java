package simulation.schedulingAlgorithm;

import simulation.dataStructures.LinkedList;
import simulation.taskGenerator.Task;

import java.util.ArrayList;

public class Rotary implements SchedulingAlgorithm {
    
    private int quantum;
    
    public Rotary(int quantum){
        this.quantum = quantum;
    }
    
    @Override
    public ArrayList<Task> findNext (ArrayList<Task> list, int n) {
        ArrayList<Task> temp = new ArrayList<>();
        if(n<=0 || list == null) return temp;
    
        if( n >= list.size()){
            for(int i = 0; i < list.size(); i++){
                temp.add(list.remove(0));
            }
        }else{
            for(int i = 0; i < n; i++){
                temp.add(list.remove(0));
            }
        }
        return temp;
    }
    
    @Override
    public String getName () {
        return "Rotacyjny";
    }
    
    @Override
    public boolean isOuster () {
        return true;
    }
    
    @Override
    public Integer quantum () {
        return quantum;
    }
}
