package simulation.presenter;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Presenter {
    private int liczba_procesorow;
    private int liczba_procesow;
    private ArrayList<Algorytm> results;

    public Presenter() {
        results = new ArrayList<Algorytm>();
    }

    public int getLiczba_procesow() {
        return liczba_procesow;
    }

    public void setLiczba_procesow(int liczba_procesow) {
        this.liczba_procesow = liczba_procesow;
    }

    public void saveData(String name, Double time)  {
        DecimalFormat myFormatter = new DecimalFormat(".##");
        String data = myFormatter.format(time);
        Algorytm alg = new Algorytm(name, data);
        results.add(alg);
    }

    public int getLiczba_procesorow() {
        return liczba_procesorow;
    }

    public void setLiczba_procesorow(int liczba_procesorow) {
        this.liczba_procesorow = liczba_procesorow;
    }

    public void print() {
        String alg = "Algorytm";
        String time = "Sredni czas oczekiwania";
        int nameLength = alg.length();
        int timeLength = time.length();
        
        for(Algorytm algorytm: results){
            if(algorytm.name.length() > nameLength) nameLength =algorytm.name.length();
            if((algorytm.time).length() > timeLength) timeLength = (algorytm.time).length();
        }
    
        StringBuilder line = new StringBuilder("");
        for(int i=0; i<nameLength+timeLength+7;i++){
            line.append("-");
        }
        System.out.println(line.toString());
        System.out.printf("|%s|%s|%n", StringUtils.center(alg,nameLength+2),StringUtils.center(time,timeLength+2));
        System.out.println(line.toString());
        for (Algorytm x: results){
            System.out.printf("|%s|%s|%n",StringUtils.center(x.getName(),nameLength+2),StringUtils.center(x.getTime(),timeLength+2));
        }
        System.out.println(line.toString());
    }

    public void start() {
        boolean czyPoprawne = false;
        while (czyPoprawne!= true) {
            try {
                System.out.print("Liczba procesorów: ");
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                liczba_procesorow = Integer.parseInt(reader.readLine());
                czyPoprawne = true;
            }
            catch (Exception e) {
                System.out.println("Błąd! Liczba procesorów musi być liczbą naturalną.");
            }
        }
        czyPoprawne = false;
        while (czyPoprawne!= true) {
            try {
                System.out.print("Liczba precesów: ");
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                liczba_procesow = Integer.parseInt(reader.readLine());
                czyPoprawne = true;
            }
            catch (Exception e) {
                System.out.println("Błąd! Liczba procesów musi być liczbą naturalną.");
            }
        }
    }

    public void saveDataInFile()  {
        Writer output = null;
        try {
            output = new BufferedWriter(new FileWriter("text.txt", false));
            output.append("Symulacja algorytmów planowania dostępu " + this.getLiczba_procesow() + " procesów"+" do " + this.getLiczba_procesorow() + " procesorów: \n");

            for (Algorytm x: results) {
                ((BufferedWriter) output).newLine();
                output.append("Sredni czas dla algorytmu " + x.getName() + ": " + x.getTime() + "\n");
            }
            output.close();
        }
        catch (Exception e) {
            e.getMessage();
        }
    }
    
    private class Algorytm {
        private String name;
        private String time;
        
        public Algorytm(String name, String time) {
            this.name = name;
            this.time = time;
        }
        
        public String getName() {
            return name;
        }
        
        public void setName(String name) {
            this.name = name;
        }
        
        public String getTime() {
            return time;
        }
        
        public void setTime(String time) {
            this.time = time;
        }
    }
    
}

